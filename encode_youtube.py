 #/bin/sh
 
 #!/bin/python
#
# Script to transcode multi video & audio formats into prores & pcm for import to Davinci Resolve
#
import os
import sys
import subprocess

indir = "/home/antoine/Videos/pre_transcode_in" #Input directory for pre-transcode video & audio files
outdir= "/home/antoine/Videos/transcoded_prores_pcm/" #Output directory for transcoded prores/pcm video & audio files
#
# [ ! -d "$outdir" ] && mkdir "$outdir" #Check if output directory exists, if not, create it.
#

video_ext = ["mkv", "m4v", "mp4", "avi", "wmv", "flv", "mov"]

CRF = "23"



# BITRATE=10M # 1080p HDR fps: 24,25 or 30
# BITRATE=15M # 1080p HDR fps: 48, 50 or 60

# BITRATE=8M # 1080p SDR fps: 24,25 or 30
# BITRATE=12M # 1080p SDR fps: 48, 50 or 60



for element in os.listdir(indir):
	file_path_in = os.path.join(indir, element)
	file_path_out = os.path.join(outdir, element.split(".")[0] + ".mp4")
	
	print("****** Starting transcode of %s to %s ******" % (file_path_in, file_path_out))
	
	print(subprocess.check_output(['ffmpeg', '-i', file_path_in, '-c:v', 'libx264', '-preset', 'slow', '-profile:v', 'high', '-crf', CRF, '-coder', '1', '-pix_fmt', 'yuv420p', '-movflags', '+faststart', '-g', '30', '-bf', '2', '-c:a',  'aac', '-b:a', '384k', '-profile:a', 'aac_low', file_path_out]))

 
