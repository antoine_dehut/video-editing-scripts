 #/bin/sh
 
CRF=23

# BITRATE=10M # 1080p HDR fps: 24,25 or 30
# BITRATE=15M # 1080p HDR fps: 48, 50 or 60

# BITRATE=8M # 1080p SDR fps: 24,25 or 30
# BITRATE=12M # 1080p SDR fps: 48, 50 or 60


ffmpeg -i $1 \
-c:v libx264 -preset slow -profile:v high -crf $CRF -coder 1 -pix_fmt yuv420p -movflags +faststart -g 30 -bf 2 \
-c:a aac -b:a 384k -profile:a aac_low $2
