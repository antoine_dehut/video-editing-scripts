 
#!/bin/python
#
# Script to transcode multi video & audio formats into prores & pcm for import to Davinci Resolve
#
import os
import sys
import subprocess


rootDir = 'Musique_rm_opus/'
for dirName, subdirList, fileList in os.walk(rootDir):
	for fname in fileList:
		if(".opus" in fname or ".opus".upper() in fname):
			# delete file
			 print('Found opus: %s' % os.path.join(dirName, fname))
			 os.remove(os.path.join(dirName, fname))
